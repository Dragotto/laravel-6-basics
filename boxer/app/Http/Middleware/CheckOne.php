<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class CheckOne
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if (1===1)
            return abort(403,'Si,ritorna 1');
        return $next($request);
    }
}
