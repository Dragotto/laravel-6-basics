<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class CheckLocal
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        // IL MIDDLEWARE CONTROLLA SE SIAMO IN LOCALE O NO
        // PER TEST BASTA TOGLIERE LA NEGAZIONE !
        if(!app()->environment('local'))
            return abort(403,'Non sei in locale');
        return $next($request);
    }
}
