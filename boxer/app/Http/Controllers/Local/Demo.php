<?php

namespace App\Http\Controllers\Local;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class Demo extends Controller
{
    public function __construct()
    {
        $this->middleware('one');
    }

    public function __invoke()
    {
        //IL METODO dd() DUMP AND DIE STAMPA QUALCOSA E POI MUORE,
        //IN QUESTO CASO STAMPA LA NOSTRA CLASSE
        dd($this);
    }
}
