<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'View\LandingPage');

Route::get('/accedi', 'View\Login')->name('login');

Route::get('/local', 'Local\Demo')->name('local.demo')->middleware('local');

